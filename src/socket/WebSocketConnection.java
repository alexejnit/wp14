package socket;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.catalina.websocket.MessageInbound;
import org.apache.catalina.websocket.WsOutbound;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import de.fhwgt.quiz.application.Game;
import de.fhwgt.quiz.application.Player;
import de.fhwgt.quiz.application.Question;
import de.fhwgt.quiz.application.Quiz;
import de.fhwgt.quiz.error.QuizError;

/** 
 * SERVER -> CLIENT
 * ermöglicht Broadcast an alle Kommunikationsteilnehmer
 * per Methode onTextMessage
 * @see #onTextMessage(CharBuffer)
 * **/
public class WebSocketConnection extends MessageInbound {

	private long id;
	private Websocket socket;
	private QuizError error;
	private Collection <Player> players;
	private Quiz quiz;
	private String catalog;
	private Player superUser;
	private JSONObject type1, type3, type4, type5, type6, type7, type8, type10, jsonError, logout;
	private JSONParser parser;
	private int msgType;
	private JSONObject jsonMessage;
	private int playerId;
	
	/**
	 * Initialisiere Websocket connection
	 * @param Websocket socket
	 */
	public WebSocketConnection(Websocket socket){
		this.socket = socket;
		this.error = new QuizError();
		this.quiz = Quiz.getInstance();
		this.players = quiz.getPlayerList();
		this.jsonError = new JSONObject();
		this.parser = new JSONParser();
		this.id = 99;
	}

	@Override
	protected void onTextMessage(CharBuffer message) throws IOException {
		System.out.println("Empfange: "+message);
		try {
			Object buf = parser.parse(message.toString());
			jsonMessage = (JSONObject) buf;
			String buf2 = (String) jsonMessage.get("type");
			this.msgType = Integer.parseInt(buf2);
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
	//	System.out.println("Server: empfangene daten vom typ: "+msgType);
		
		switch (msgType) {
		// spielstart request
		case 1:
			type1 = new JSONObject();
			catalog = (String) jsonMessage.get("catalogName");
			for(Player player : players){
				if(player.isSuperuser()){
					this.superUser = player;
					quiz.changeCatalog(superUser, catalog, error);
					if(error.isSet()){
						System.out.println("change catalog error: "+error.getDescription()+" status code: "+error.getStatus());
					}				
					if(quiz.startGame(superUser, error)){
//						System.out.println("Spiel ist gestartet!");
						type1.put("type", 1);
						this.broadcast(type1.toString());	
					}else{
						System.out.println("error: "+error.getDescription());
						System.out.println("error status: "+error.getStatus());
						jsonError.put("type", 0);
						jsonError.put("fatal", 0);
						jsonError.put("fehlerNachricht", error.getDescription());
						this.send(jsonError.toString());
					}
				}
			}
			break;
 
		// question request
		case 2:
			try {
				Object buf = parser.parse(message.toString());
				jsonMessage = (JSONObject) buf;
				String buf2 = jsonMessage.get("id").toString();
				this.playerId = Integer.parseInt(buf2);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(Player player : players){	
				if(player.getId() == playerId){
					Timeout timeout = new Timeout(this, player);
					Question question = quiz.requestQuestion(player, timeout, error);
					if(error.isSet()){
						System.out.println("request question error: "+error.getDescription()+" status code: "+error.getStatus());
					}
					// alle fragen Beantwortet -> spielende
					if(question == null){
//						System.out.println("spielende");
						long score = player.getScore();
						type6 = new JSONObject();
						type6.put("type", 6);
						type6.put("score", score);
						this.send(type6.toJSONString());
						boolean gameIsOver = quiz.setDone(player);
						if(gameIsOver){ // sende Rang an Spieler
							type7 = new JSONObject();
							type7.put("type", 7);
							List<Player> list = new ArrayList<Player>(players);
							Comparator<Player> compare = new ComparePlayers();
							Collections.sort(list, compare);
							int counter = 1;
							for(Player pl : list){
//								System.out.println("id"+pl.getId()+ counter);
								type7.put("id"+pl.getId(), counter);
								counter = counter + 1;
							}
							this.broadcast(type7.toString());
							this.quiz.setGameOver();
							
							/* Nach Spielende alle Spieler disconnecten, damit ein neues Spiel begonnen werden kann */				
							ArrayList<WebSocketConnection> con = Websocket.getAllConnections();
							for(WebSocketConnection connections : con){
								connections.getWsOutbound().close(1000, null);
							}	
							Websocket.closeAllWebsockets();
							int listCount = Websocket.socketList.size();
							System.out.println("closeAllWebsockets: Anzahl verbindungen:" +listCount);
						}
					} else{	
						type3 = new JSONObject();
						type3.put("type", 3);
						type3.put("question", question.getQuestion());
						type3.put("answer", question.getAnswerList());
						type3.put("timeout", question.getTimeout());
						this.send(type3.toString());
					}
				}
			}
			break;
		
		// client answer 
		case 4:	
			try {
				Object buf = parser.parse(message.toString());
				jsonMessage = (JSONObject) buf;
				String buf2 = jsonMessage.get("id").toString();
				this.playerId = Integer.parseInt(buf2);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(Player player : players){	
				if(player.getId() == playerId){
					type5 = new JSONObject();
					long playerAnswer = (Long) this.jsonMessage.get("answer");
					long rightAnswer;
					rightAnswer = this.quiz.answerQuestion(player, playerAnswer, error);
					if(error.isSet()){
						System.out.println("answerQuestion error: "+error.getDescription()+" status code: "+error.getStatus());	
					}
					if(rightAnswer != -1){
						type5.put("type", 5);
						type5.put("rightAnswer", rightAnswer);
						this.send(type5.toJSONString());
						this.quiz.signalPlayerChange();
					} else{System.out.println("error in case4: answerQuestion");}
				}
			}
			break;
			
		// catalog changed
		case 8:
			type8 = new JSONObject();
			catalog = (String) jsonMessage.get("catalogName");
			type8.put("type", 9);
			type8.put("catalogName", catalog);
			this.broadcast(type8.toString());
			break;	
		
		//Login
		case 10:
			type10 = new JSONObject();
			String playerName = (String) jsonMessage.get("playerName");
			Player newPlayer = quiz.createPlayer(playerName, error);
			if(error.isSet())
			{
				type10.put("type", 0);
				type10.put("fatal", 1);
				type10.put("fehlerNachricht", error.getDescription());
				this.send(type10.toString());
				error = new QuizError();
			}else
			{
				type10.put("type", 11);
				type10.put("playerName", newPlayer.getName());
				type10.put("id", newPlayer.getId());
				this.send(type10.toString());
				this.id = newPlayer.getId();
			}
			break;
		
		default:
			break;
		}		
	}
	
	/**
	 * Broadcastet übergebenen String an alle verbundenen Clients
	 * @param message String
	 * @throws IOException
	 */
	private void broadcast(String message) throws IOException{	
		int listCount = Websocket.socketList.size();
		System.out.println("Anzahl verbindungen:" +listCount+ " broadcast: "+message);
		WebSocketConnection con;
		for (int i = 0; i < listCount; i++) {
			con = Websocket.socketList.get(i);
			CharBuffer buf = CharBuffer.wrap(message);
			con.getWsOutbound().writeTextMessage(buf);
		}
	}
	
	/**
	 * Sendet die Richtige Antwort an den jeweiligen @code{player} zurück, falls dieser die
	 * Frage nicht innerhalb des Zeitfensters der jeweiligen Frage beantwortet hat.
	 * @param con
	 * @param player
	 */
	public void timeout(WebSocketConnection con, Player player){
		type5 = new JSONObject();
		long index = quiz.answerQuestion(player, 1, error);
		if(error.isSet()){
			System.out.println("timeout error: "+error.getDescription()+" status code: "+error.getStatus());
		} else{
			type5.put("type", 5);
			type5.put("rightAnswer", index);
			try {
				this.send(type5.toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				System.out.println("Fehler beim Senden nach Timeout");
			}
		}
	}
	
	/**
	 * Sendet Nachricht an einen einzelnen Client
	 * @param message String
	 * @throws IOException
	 */
	private void send(String message) throws IOException{
		System.out.println("sende: "+message);
		CharBuffer buf = CharBuffer.wrap(message);
		this.getWsOutbound().writeTextMessage(buf);
	}
	
	@Override
	public void onOpen(WsOutbound outbound){}
	
	@Override
	public void onClose(int status){
		
		System.out.println("Socket schlie�t: " + status);
		//Wenn sich dder Spieler angemeldet hat
		if(id != 99){
			logout = new JSONObject();
			Player logoutPlayer = null;
			for(Player player: Quiz.getInstance().getPlayerList()){
				if(player.getId() == id){
					logoutPlayer = player;
					System.out.println("Enferne "+ logoutPlayer.getName());
				}
			}		
			//Entferne Socket
			Websocket.removeWebsocket(this);
			
			//Entferne Spieler
			if(logoutPlayer != null){
				Quiz.getInstance().removePlayer(logoutPlayer, error);
			}
	
			if(error.isSet())
			{
				System.out.println("LogoutError " + error.getDescription());
				logout.put("type", 0);
				logout.put("fatal", 1);
				logout.put("fehlerNachricht", error.getDescription());
				
				try {
					this.broadcast(logout.toString());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				// resettet quiz error 
				error = new QuizError();
			}
			
		}else
		{//Hat sich der Spieler garnicht angemeldet
			Websocket.removeWebsocket(this);
		}
		
	}
	
	@Override
	protected void onBinaryMessage(ByteBuffer arg0) throws IOException {}
}
	
