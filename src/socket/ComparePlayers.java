package socket;

import java.util.Comparator;

import de.fhwgt.quiz.application.Player;

public class ComparePlayers implements Comparator<Player>{

        @Override
        public int compare(Player o1, Player o2) {
                return (int) (o2.getScore() - o1.getScore());
        }  
}