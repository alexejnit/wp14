package socket;

import java.util.TimerTask;

import de.fhwgt.quiz.application.Player;


public class Timeout extends TimerTask{
       
        WebSocketConnection con;
        Player player;
       
        public Timeout(WebSocketConnection con, Player player) {
                this.con = con;
                this.player = player;
        }
       
        @Override
        public void run() {
                System.out.println("Zeit abgelaufen");
                this.con.timeout(con, player);
        }

}
