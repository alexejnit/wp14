package socket;

import java.util.ArrayList;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;

import org.apache.catalina.websocket.StreamInbound;
import org.apache.catalina.websocket.WebSocketServlet;

/** 
 * Manager-Klasse aller bestehenden Client-Verbindungen;
 * @param ArrayList
 * @method StreamInbound(string, HttpServletRequest)
 * @return WebSocketConnection
 **/

@WebServlet("/Websocket")
public class Websocket extends WebSocketServlet{

	private static final long serialVersionUID = 1L;
	public static ArrayList<WebSocketConnection> socketList = new ArrayList<WebSocketConnection>(); // Vorsicht unsynchronisiert!
	
	@Override
	protected StreamInbound createWebSocketInbound(String arg0,
			HttpServletRequest arg1) {
		WebSocketConnection con = new WebSocketConnection(this);

		if(socketList.add(con))
		{
			System.out.println("Socket angelegt Anzahl der Verbindungen: "+socketList.size());
		}	
		return con;
	}
	
	/**
	 * Synchronisierter Zugriff auf die Connection-Liste
	 * @return String
	 */
	public static synchronized String outputToAllConnections(){
		return socketList.toString();
	}
	
	/**
	 * Synchronisierter Zugriff auf WebSocketConnection ArrayListe
	 * @return ArrayList
	 */
	public static synchronized ArrayList<WebSocketConnection> getAllConnections(){
		return socketList;
	}
	
	public static synchronized void removeWebsocket(WebSocketConnection _socket)
	{
		socketList.remove(_socket);
	}
	
	public static synchronized void closeAllWebsockets(){
		socketList.removeAll(socketList);
		socketList.retainAll(socketList);
	}
}
