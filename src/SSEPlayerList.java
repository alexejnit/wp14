

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import de.fhwgt.quiz.application.Player;
import de.fhwgt.quiz.application.Quiz;

/**
 * Servlet implementation class SSEPlayerList
 */
@WebServlet("/SSEPlayerList")
public class SSEPlayerList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SSEPlayerList() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/event-stream");
		response.setCharacterEncoding("UTF-8");
		
		response.setHeader("pragma", "no-cache,no-store");  
		response.setHeader("cache-control", "no-cache,no-store,max-age=0,max-stale=0");
		
		Collection<Player> list = Quiz.getInstance().getPlayerList();
		
		PrintWriter pw = response.getWriter();
		JSONArray playerArrayJSON = new JSONArray();
		
		for(Player i : list)
		{
			JSONObject playerJSON = new JSONObject();
			playerJSON.put("name", i.getName());
			playerJSON.put("score", i.getScore());
			playerArrayJSON.add(playerJSON);
		}
		System.out.println(playerArrayJSON.toString());
		pw.write("data: " + playerArrayJSON.toString() +"\n\n");
		pw.flush();
		pw.close();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

}
