
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import de.fhwgt.quiz.application.Catalog;
import de.fhwgt.quiz.application.Quiz;
import de.fhwgt.quiz.loader.FilesystemLoader;
import de.fhwgt.quiz.loader.LoaderException;

/**
 * Servlet implementation class AJAXCatalogServlet
 */
@WebServlet("/AJAXCatalogServlet")
public class AJAXCatalogServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private FilesystemLoader loader;
	private Map<String, Catalog> catalogs;   
	private Quiz quiz;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AJAXCatalogServlet() {
        super();
        this.loader = new FilesystemLoader("Catalogs");
        quiz = Quiz.getInstance();
        quiz.initCatalogLoader(loader);
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/xml");
		PrintWriter writer=response.getWriter();
		String xmlCatalogs = "<message><catalogs>";
		
		try {
			this.catalogs = quiz.getCatalogList();
			for(String i : catalogs.keySet())
			{
				xmlCatalogs= xmlCatalogs + "<catalog>" + catalogs.get(i).getName() +"</catalog>";
//				System.out.println(catalogs.get(i).getName()+" zur xml hinzugefügt");
			}
			xmlCatalogs = xmlCatalogs + "</catalogs></message>";
		} catch (LoaderException e) {
			e.printStackTrace();
		}
		writer.print(xmlCatalogs);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {}

}
