Legende

+--------+----------+
|type | Daten |
+--------+----------+

********************************************************************************

Spiel starten (Request)

type: 1

Richtung: Server <== Client

+--------+-------------------------+
|type | Dateiname des Katalogs |
+--------+-------------------------+

Beispiel: 1Simple.xml

********************************************************************************

Spiel starten (Response)

type: 1

Richtung: Server ==> Client

+--------+-------+
|type | Leer |
+--------+-------+

Beispiel: 1

********************************************************************************

Frage (Request)

type: 2

Richtung: Server <== Client

+--------+-------+
|type | PlayerID |
+--------+-------+

Beispiel: {"typee":"2","id","0"}

********************************************************************************

Frage (Response)

type: 3

Richtung: Server ==> Client

+--------+-----------------------+
|type | Frage als JSONObjekt |
+--------+-----------------------+

JSONObjekt: 
typee: 2
frage: "......"
antwort: "....."
timeout: "....."

Beispiel: {"type":"3","frage":"Der Systemaufruf fork...",
"antwort":["1. Antwort","2. Antwort","3. Antwort","4. Antwort"],
"timeout":20000}

********************************************************************************

Ausgewählte Antwort

type: 4

Richtung: Server <== Client

+--------+----------------------+
|type | Antwort JSONObject |
+--------+----------------------+

type: 4
Antwort: 0 - 3
id: 0-6

Beispiel: {"type":4,"answer":2,"id": 0}

********************************************************************************

Richtige Antwort

type: 5

Richtung: Server ==> Client

+--------+---------------------------------+
|type | Richtige Antwort als JSONObjekt |
+--------+---------------------------------+

typee: 5
rightAnswer: 0 - 3

Beispiel: {"type":"5","rightAnswer":0}

********************************************************************************

Spielende

type: 6

Richtung: Server ==> Client

+--------+--------------------------+
|type | Spielende als JSONObjekt |
+--------+--------------------------+

JSONObjekt: type: 6
score: beliebig

Beispiel: {"type":"6","score":3}

********************************************************************************

Rang

type: 7

Richtung Server ==> Client

+--------+------------------------------------+
|type | Rang allen Speiler als JSONObjekt |
+--------+------------------------------------+

JSONObjekt: type: 7
id0 - 5: 1 - 6

Beispiel: {"type":"7","id2":3,"id1":2,"id0":1}

********************************************************************************

Katalogauswahl (Client)

Richtung Server <== Client

+--------+---------------------+
|type | Ausgewählte Katalog |
+--------+---------------------+

String: type: 8
Ausgewählte Katalog: 1 - 3;

Beispiel: "83"

********************************************************************************

Katalogauswahl (Server)

Richtung Server ==> Client

+--------+------------------------------------+
|type | Ausgewählte Katalog an alle Clients|
+--------+------------------------------------+

JSONObjekt type: 9
Ausgewählte Katalog: 1 - 3

Beispiel {"type":"9","cat":"2"}

********************************************************************************

Fehler

type: 0

Richtung: Server ==> Client

+--------+-----------------------+
|type | Fehler als JSONObjekt |
+--------+-----------------------+

JSONObjekt: type: 0
fatal: ja -> 1 nein -> 0
fehlerNachricht: "....."

Beispiel: {"type":"0","fatal":"0","fehlerNachricht":"Kein zweite Spieler"}

********************************************************************************