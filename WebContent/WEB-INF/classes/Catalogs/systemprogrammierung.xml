<?xml version="1.0" encoding="UTF-8"?>
<!-- <!DOCTYPE katalog SYSTEM "katalog.dtd"> -->
<?xml-stylesheet type="text/xsl" href="katalog.xsl" ?>

<katalog name="simple" anzFragen="12"
			xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="./katalog.xsd">
	<frage id="1">
		<bezeichnung>Ein Thread soll auf ein durch einen anderen Thread ausgelöstes Ereignis warten. Welcher Mechanismus ist geeignet?</bezeichnung>
		<timeout>30</timeout>
		<antworten>
			<antwortRichtig>nur Semaphore</antwortRichtig>
			<antwortFalsch1>nur Mutex</antwortFalsch1>
			<antwortFalsch2>weder Semaphore noch Mutexe</antwortFalsch2>
			<antwortFalsch3>sowohl Semaphore als auch Mutexe</antwortFalsch3>
		</antworten>
	</frage>

	<frage id="2">
		<bezeichnung>Der Systemaufruf fork...</bezeichnung>
		<timeout>20</timeout>
		<antworten>
			<antwortRichtig>...erzeugt einen neuen Prozess, der das gleiche Programm ausführt.</antwortRichtig>
			<antwortFalsch1>...erzeugt einen neuen Prozess, der ein anderes Programm ausführt.</antwortFalsch1>
			<antwortFalsch2>...erzeugt einen neuen Thread.</antwortFalsch2>
			<antwortFalsch3>...ist das logische Gegenstück zum Aufruf knife.</antwortFalsch3>
		</antworten>
	</frage>

	<frage id="3">
		<bezeichnung>In einem Signalhandler...</bezeichnung>
		<timeout>30</timeout>
		<antworten>
			<antwortRichtig>...dürfen nur bestimmte Funktionen aufgerufen werden.</antwortRichtig>
			<antwortFalsch1>...kann man problemlos alles machen, was man auch im regulären Programmcode macht.</antwortFalsch1>
			<antwortFalsch2>...dürfen keine Fließkommaoperationen durchgeführt werden.</antwortFalsch2>
			<antwortFalsch3>...ist nur der Systemaufruf write verboten.</antwortFalsch3>
		</antworten>
	</frage>

	<frage id="4">
		<bezeichnung>Sie möchten eine Datenstruktur vor gleichzeitigem Zugriff aus mehreren Threads schützen. Welcher Mechanismus ist dafür geeignet?</bezeichnung>
		<timeout>30</timeout>
		<antworten>
			<antwortRichtig>sowohl Semaphore als auch Mutexe</antwortRichtig>
			<antwortFalsch1>nur Semaphore</antwortFalsch1>
			<antwortFalsch2>nur Mutexe</antwortFalsch2>
			<antwortFalsch3>nur Signale</antwortFalsch3>
		</antworten>
	</frage>
	
	<frage id="5">
		<bezeichnung>Es soll darauf gewartet werden, dass Daten eintreffen, allerdings nur eine bestimmte Zeit. Wie kann man das umsetzen?</bezeichnung>
		<timeout>30</timeout>
		<antworten>
			<antwortRichtig>mit dem Systemaufruf select</antwortRichtig>
			<antwortFalsch1>durch einen zusätzlichen Timeout-Parameter bei read oder recv</antwortFalsch1>
			<antwortFalsch2>gar nicht</antwortFalsch2>
			<antwortFalsch3>durch Setzen des Timeout-Status mit fcntl</antwortFalsch3>
		</antworten>
	</frage>
	
	<frage id="6">
		<bezeichnung>Mit welchem Systemaufruf kann man den Port festlegen, auf dem ein Serverprogramm auf Verbindungen wartet?</bezeichnung>
		<timeout>25</timeout>
		<antworten>
			<antwortRichtig>bind</antwortRichtig>
			<antwortFalsch1>accept</antwortFalsch1>
			<antwortFalsch2>setsock</antwortFalsch2>
			<antwortFalsch3>open</antwortFalsch3>
		</antworten>
	</frage>
	
	<frage id="7">
		<bezeichnung>Während eines read-Aufrufs tritt ein Signal auf und wird durch einen Signalhandler abgearbeitet (SA_RESTART nicht gesetzt). Was passiert?</bezeichnung>
		<timeout>45</timeout>
		<antworten>
			<antwortRichtig>Der Aufruf kehrt unter Umständen mit weniger Daten als angefordert oder EINTR zurück.</antwortRichtig>
			<antwortFalsch1>Es werden auf jeden Fall so viele Daten gelesen wie angefordert.</antwortFalsch1>
			<antwortFalsch2>Die Daten gehen verloren.</antwortFalsch2>
			<antwortFalsch3>Das Programm wird in solchen Fällen vom Betriebssystem abgebrochen.</antwortFalsch3>
		</antworten>
	</frage>
	
	<frage id="8">
		<bezeichnung>Beim Lesen aus einer Pipe meldet der Aufruf read das Dateiende, wenn...</bezeichnung>
		<timeout>20</timeout>
		<antworten>
			<antwortRichtig>...alle Schreib-Deskriptoren für die Pipe geschlossen und alle Daten aus der Pipe gelesen wurden.</antwortRichtig>
			<antwortFalsch1>...der Leser ein spezielles Signal erhält.</antwortFalsch1>
			<antwortFalsch2>...der schreibende Prozess fork aufruft.</antwortFalsch2>
			<antwortFalsch3>...^D in die Pipe geschrieben wird.</antwortFalsch3>
		</antworten>
	</frage>
	
	<frage id="9">
		<bezeichnung>Was passiert mit einem Shared-Memory-Objekt, wenn es nicht manuell gelöscht wird?</bezeichnung>
		<timeout>30</timeout>
		<antworten>
			<antwortRichtig>Es bleibt weiterhin bestehen, bis es von Hand oder durch einen Reboot gelöscht wurde.</antwortRichtig>
			<antwortFalsch1>Es wird zerstört, wenn sich der Prozess beendet, der es erzeugt hat.</antwortFalsch1>
			<antwortFalsch2>Es wird zerstört, wenn kein Prozess mehr auf es zugreift.</antwortFalsch2>
			<antwortFalsch3>Der Garbage-Collector des Betriebssystems zerstört es, wenn der Speicher knapp wird.</antwortFalsch3>
		</antworten>
	</frage>
	
	<frage id="10">
		<bezeichnung>Welches dieser Signale kann nicht abgefangen werden?</bezeichnung>
		<timeout>20</timeout>
		<antworten>
			<antwortRichtig>SIGKILL</antwortRichtig>
			<antwortFalsch1>SIGTERM</antwortFalsch1>
			<antwortFalsch2>SIGINT</antwortFalsch2>
			<antwortFalsch3>SIGPIPE</antwortFalsch3>
		</antworten>
	</frage>
	
	<frage id="11">
		<bezeichnung>Was ist ein Zombie-Prozess unter Unix?</bezeichnung>
		<timeout>30</timeout>
		<antworten>
			<antwortRichtig>Ein Prozess, dessen Exit-Status noch nicht mittels wait abgefragt wurde.</antwortRichtig>
			<antwortFalsch1>Ein Daemon-Prozess, der durch den Aufruf zombie immun gegen das KILL-Signal gemacht wurde.</antwortFalsch1>
			<antwortFalsch2>Ein Prozess, der nur nachts gestartet werden kann.</antwortFalsch2>
			<antwortFalsch3>Ein Computervirus.</antwortFalsch3>
		</antworten>
	</frage>
	
	<frage id="12">
		<bezeichnung>Welches Signal (sofern nicht ignoriert) empfängt ein Elternprozess, wenn sich einer seiner Kindprozesse beendet?</bezeichnung>
		<timeout>20</timeout>
		<antworten>
			<antwortRichtig>SIGCHLD</antwortRichtig>
			<antwortFalsch1>SIGSYS</antwortFalsch1>
			<antwortFalsch2>SIGSEGV</antwortFalsch2>
			<antwortFalsch3>SIGABRT</antwortFalsch3>
		</antworten>
	</frage>
	
	
</katalog>
