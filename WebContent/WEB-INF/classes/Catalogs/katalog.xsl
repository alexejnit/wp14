<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
	<html>
		<head>
		</head>
		<body>
			<h3>Katalogname: <xsl:value-of select="katalog/@name"/>.cat</h3>
		<ul>
		<xsl:for-each select="katalog/frage">
			<li><xsl:value-of select="bezeichnung"/></li>
			<ul>
				<li><xsl:value-of select="antworten/antwortRichtig"/></li>
				<li><xsl:value-of select="antworten/antwortFalsch1"/></li>
				<li><xsl:value-of select="antworten/antwortFalsch2"/></li>
				<li><xsl:value-of select="antworten/antwortFalsch3"/></li>
			</ul>
		</xsl:for-each>
		</ul>
		</body>
	</html>
	</xsl:template>
</xsl:stylesheet>