/**
 * 
 */
function send(event){

	var playButton = document.getElementById("playButton");
	var loginButton = document.getElementById("loginButton");
	var choosenKatalogLabel = null;
	var _choosenCatalog = quiz.getChoosenCatalog();
	
	if(_choosenCatalog != null)
	{
		var catalogLabel = document.getElementsByName("katalogNamen");
		for(var i = 0; i < catalogLabel.length; i++)
		{
			
			if(_choosenCatalog.innerHTML == catalogLabel[i].innerHTML) 
			{
				choosenKatalogLabel = catalogLabel[i];
			}
		}
	}
	if(!quiz.isGameStarted()){
	
		switch(event.target)
		{
			case loginButton :
				if(document.getElementById("loginTextField").value == "" || document.getElementById("loginTextField").value == "username")
				{
					alert("Bitte Usernamen Eingeben!");
				}else
				{
					var loginJSON = { type: "10",
							  playerName : document.getElementById("loginTextField").value};
					quiz.getSocket().send(JSON.stringify(loginJSON));
					console.log("Speieler Namen gesendet: " + document.getElementById("loginTextField").value);
				}
				
				break;
			case playButton:
				if(_choosenCatalog == null) 
				{
					alert("Biite Katalog ausw�hlen!");
				}
				else
				{
					var gameStartJSON = { type: "1",
										 	catalogName: choosenKatalogLabel.innerHTML};
					quiz.getSocket().send(JSON.stringify(gameStartJSON));
				}
				break;
				
			case choosenKatalogLabel:
				var catalogChangedJSON = { type: "8", 
											catalogName: choosenKatalogLabel.innerHTML };
				quiz.getSocket().send(JSON.stringify(catalogChangedJSON));
				break;
				
			default : console.log("Unbekennte Sende Operetion");
		}
	}else
	{
		var messageJSON;
		switch(event)
		{
			case "getQuestion":
				
				messageJSON = { type: "2", id: quiz.getPlayerID()};
				quiz.getSocket().send(JSON.stringify(messageJSON));
				console.log("Kataloganfrage gesendet Typ: " +messageJSON.type + " ID: " + messageJSON.id);
				break;
			    
			case "selectedAnswer":
				var selectedAnswer = quiz.getChoosenAnswer();
				messageJSON = { type: "4", answer: selectedAnswer, id: quiz.getPlayerID()};
				quiz.getSocket().send(JSON.stringify(messageJSON));
				break;
				
			default : console.log("Unbekennte Sende Operetion"+event);
		}
	}
}