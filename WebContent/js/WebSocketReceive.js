/**
 * 
 */
function receive(message){
	
	var messageJSON = JSON.parse(message.data);
	console.log(">>>>>>Nachricht empfangen: "+ JSON.stringify(messageJSON));

	switch (messageJSON.type)
	{
		case 0:
			if(messageJSON.fatal != 0 )
			{
				alert("FatalError: "+messageJSON.fehlerNachricht);
				console.log("FatalError: "+messageJSON.fehlerNachricht);
				startNewGame();
//				window.setTimeout("startNewGame()", 2000);
			}else
			{
				alert(messageJSON.fehlerNachricht);
				console.log(messageJSON.fehlerNachricht);
			}
			break;
		case 1:
			quiz.setGameStarted(true);
			console.log("Empfange Typ: "+ messageJSON.type);
			send("getQuestion");
			
			var catalogs = document.getElementsByName("katalogNamen");
			for(var i = 0; i < catalogs.length; i++)
			{
				catalogs[i].removeEventListener("click", setRedText, false);
				catalogs[i].removeEventListener("mouseover", setGreyText, false);
				catalogs[i].removeEventListener("mouseout", setBlackText, false);
				catalogs[i].removeEventListener("click", send, false);
			}
			break;
			
		case 3:
			var playArea = document.getElementById("playArea_login");
			if(quiz.isFirstQuestion())
			{
				if(quiz.isSuperUser())
				{
					playArea.removeChild(document.getElementById("playButton"));
				}
				playArea.removeChild(document.getElementById("textArea"));
				quiz.setFirstQuestion(false);
			}
			
			document.getElementById("headLine").innerHTML = messageJSON.question;
			var answerArea = document.createElement("div");
			answerArea.setAttribute("style", "position:absolute; left: 2%; top: 30%; height: 50%;");
			answerArea.setAttribute("id", "answerArea");
			
			for(var i = 0; i < messageJSON.answer.length; i++)
			{
				var answer = document.createElement("label");
				answer.appendChild(document.createTextNode(messageJSON.answer[i]));
				answer.setAttribute("name", "Answers");

				answer.addEventListener("mouseover", setGreyText, false);
				answer.addEventListener("mouseout", setBlackText, false);
				answer.addEventListener("click", sendAnswer, false);
				answerArea.appendChild(answer);
				answerArea.appendChild(document.createElement("br"));
			}
			playArea.appendChild(answerArea);
			
			timeOut(messageJSON.timeout);
			break;
			
		case 5:
			for(var i = 0; i < document.getElementsByName("Answers").length; i++)
			{
				document.getElementsByName("Answers")[i].removeEventListener("mouseout", setBlackText, false);
				document.getElementsByName("Answers")[i].removeEventListener("mouseover", setGreyText, false);
				document.getElementsByName("Answers")[i].removeEventListener("click", setRedText, false);
			}
			if(quiz.getChoosenAnswer() == messageJSON.rightAnswer)
			{
				document.getElementsByName("Answers")[quiz.getChoosenAnswer()].setAttribute("style", "color: #82FA58;");
				quiz.setChoosenAnswer(null);
			}else
			{
				document.getElementsByName("Answers")[messageJSON.rightAnswer].setAttribute("style", "color: #82FA58;");
				if(quiz.getChoosenAnswer() != null)
				{
					document.getElementsByName("Answers")[quiz.getChoosenAnswer()].setAttribute("style", "color: red;");
				}
				quiz.setChoosenAnswer(null);
			}
			window.setTimeout("clearPlayArea()", 2000);
			break;
		
		case 6:
			document.getElementById("headLine").innerHTML = "";
			var endText = document.createElement("P");
			endText.setAttribute("id", "endText");
			endText.innerHTML = "Du hast " + messageJSON.score + " Punkte erreicht. Einige Spieler scheinen noch nicht fertig zu sein..";
			document.getElementById("playArea_login").appendChild(endText);
			break;
			
		case 7:
			var pName = quiz.getPlayerName();
			console.log("Im case 7 ID: " +quiz.getPlayerID());
			switch(quiz.getPlayerID())
			{
				case 0:
					document.getElementById("playArea_login").removeChild(document.getElementById("endText"));
					document.getElementById("headLine").innerHTML = pName + " Du hast den " +messageJSON.id0 + ". Rang belegt";
					console.log("id 0 hat Rang "+messageJSON.id0);
					break;
				case 1:
					document.getElementById("playArea_login").removeChild(document.getElementById("endText"));
					document.getElementById("headLine").innerHTML = pName + " Du hast den " +messageJSON.id1 + ". Rang belegt";
					console.log("id 1 hat Rang "+messageJSON.id1);
					break;
					
				case 2:
					document.getElementById("playArea_login").removeChild(document.getElementById("endText"));
					document.getElementById("headLine").innerHTML = pName + " Du hast den " +messageJSON.id2 + ". Rang belegt";
					break;
					
				case 3:
					document.getElementById("playArea_login").removeChild(document.getElementById("endText"));
					document.getElementById("headLine").innerHTML = pName + " Du hast den " +messageJSON.id3 + ". Rang belegt";
					break;
					
				case 4:
					document.getElementById("playArea_login").removeChild(document.getElementById("endText"));
					document.getElementById("headLine").innerHTML = pName + " Du hast den " +messageJSON.id4 + ". Rang belegt";
					break;
					
				case 5:
					document.getElementById("playArea_login").removeChild(document.getElementById("endText"));
					document.getElementById("headLine").innerHTML = pName + " Du hast den " +messageJSON.id5 + ". Rang belegt";
					break;
				
				case 6:
					document.getElementById("playArea_login").removeChild(document.getElementById("endText"));
					document.getElementById("headLine").innerHTML = pName + " Du hast den " +messageJSON.id6 + ". Rang belegt";
					break;
				default: console.log("id wurde nicht erkannt");
					break;
			}
			var newGameButton = document.createElement("input");
			newGameButton.setAttribute("id", "newGameButton");
			newGameButton.setAttribute("type", "button");
			newGameButton.setAttribute("value", "Neues Spiel");
			newGameButton.setAttribute("style", "position:absolute; left: 2%; top: 40%; ");
			newGameButton.addEventListener("click", startNewGame, false);
			document.getElementById("playArea_login").appendChild(newGameButton);
			
//			for( var i = 0; i < document.getElementsByName("PlayerListPlayer").length; i++)
//			{
//				document.getElementsByName("PlayerListPlayer").innerHTML = "";
//				document.getElementsByName("score").innerHTML = "";
//			}
			break;
			
		case 9:
			for(var i = 0; i < document.getElementsByName("katalogNamen").length; i++)
			{
				if(messageJSON.catalogName == document.getElementsByName("katalogNamen")[i].innerHTML)
				{
					for(var j = 0; j < document.getElementsByName("katalogNamen").length; j++)
					{
						document.getElementsByName("katalogNamen")[j].setAttribute("style", "color: black;");
					}
					document.getElementsByName("katalogNamen")[i].setAttribute("style", "color: red;");
				}
			}
			break;
			
		//Login	
		case 11:
			playerList();
			quiz.setPlayerName(messageJSON.playerName);
			quiz.setPlayerID(messageJSON.id);
			
			var playArea = document.getElementById("playArea_login");
			var textField = document.getElementById("loginTextField");
			var loginButton = document.getElementById("loginButton");
			var playButton;
			playArea.removeChild(textField);
			var headLine = document.createElement("h4");
			headLine.setAttribute("style", "position:absolute; left: 2%;");
			headLine.setAttribute("id", "headLine");
			
			var headLineText = document.createTextNode("Hallo " + quiz.getPlayerName());
			
			var textArea = document.createElement("p");
			textArea.setAttribute("id", "textArea");
			textArea.setAttribute("style", "position:absolute; left: 2%; top: 15%;");
			
			playArea.removeChild(loginButton);
			
			if(quiz.getPlayerID() == "0")
			{
				quiz.setSuperuser(true);
				var text = document.createTextNode("Du bist Spielleiter Wähle bitte ein Katalog aus und starte das Spiel");
				playButton = document.createElement("input");
				playButton.setAttribute("id", "playButton");
				playButton.setAttribute("type", "button");
				playButton.setAttribute("value", "spielen");
				playButton.setAttribute("style", "position:absolute; left: 2%; top: 40%; width:15%;  height:10%; ");
				playButton.addEventListener("click", send, false);
				playArea.appendChild(playButton);
				
				for(var i = 0; i < document.getElementsByName("katalogNamen").length; i++)
				{
					document.getElementsByName("katalogNamen")[i].addEventListener("click", setRedText, false);
					document.getElementsByName("katalogNamen")[i].addEventListener("mouseover", setGreyText, false);
					document.getElementsByName("katalogNamen")[i].addEventListener("mouseout", setBlackText, false);
					document.getElementsByName("katalogNamen")[i].addEventListener("click", send, false);
				}
			}else
			{
				quiz.setSuperuser(false);
				var text = document.createTextNode("Bitte Warte bis Spielleiter das Spiel gestartet hat");
			}
			headLine.appendChild(headLineText);
			textArea.appendChild(text);
			playArea.appendChild(headLine);
			playArea.appendChild(textArea);
			break;
			
		default: console.log("Unbekannte Message erhalten: " +JSON.stringify(messageJSON));
		
	}
}

function timeOut(msec)
{
	var time = msec/1000;
	var playArea = document.getElementById("playArea_login");
	var timer = document.createElement("label");
	timer.setAttribute("id", "timeArea");
	timer.setAttribute("style", "position:absolute; left: 2%; top: 85%; color: black;");
	timer.innerHTML = time;
	playArea.appendChild(timer);
	
	setInterval(function(){
		if(time == 1) tooLate(timer);
		else{
			time = time-1;
			timer.innerHTML = time;
		}
	},1000);
	
	
}

function tooLate(_timer)
{
	_timer.setAttribute("style", "position:absolute; left: 2%; top: 85%; color: red;");
	_timer.innerHTML = "ZEIT ABGELAUFEN!!!";
}

function sendAnswer(event)
{
	for(var i = 0; i < document.getElementsByName("Answers").length ; i++)
	{
		if(event.target.innerHTML == document.getElementsByName("Answers")[i].innerHTML)
		{
			quiz.setChoosenAnswer(i);
			console.log("Ausgewaelte Antwort: " +i);
		}
	}
	
	send("selectedAnswer");
}

function clearPlayArea()
{
	document.getElementById("playArea_login").removeChild(document.getElementById("answerArea"));
	document.getElementById("playArea_login").removeChild(document.getElementById("timeArea"));

	send("getQuestion");
}

function startNewGame ()
{
	location.reload();
}