/**
 * 
 */
document.addEventListener("DOMContentLoaded", catalogInquiry, false);
function catalogInquiry()
{
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
		request=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
		request=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	request.onreadystatechange=insertCatalogs;
	request.open("GET", "AJAXCatalogServlet", true);
	request.send("");
}

function insertCatalogs()
{
	if( request.readyState== 4 && request.status == 200)
	{
		var catalogName = request.responseXML.getElementsByTagName("catalog");
//		console.log("Katalog empfangen: " + catalogName[0].firstChild.nodeValue);
		
		for(var i = 0; i < catalogName.length; i++)
		{
			quiz.setCatalogSelection(document.getElementById("katalogAuswahl"));
			
			var catalog = document.createElement("label");
			catalog.setAttribute("class", "kataloge");
			catalog.setAttribute("name", "katalogNamen");
			
			var katalogName = document.createTextNode(catalogName[i].firstChild.nodeValue);
			
			catalog.appendChild(katalogName);
			
			quiz.getCatalogSelection().appendChild(catalog);
			quiz.getCatalogSelection().appendChild(document.createElement("br"));
		}
	}
}

function setRedText(event)
{
	event.target.removeEventListener("mouseout", setBlackText, false);
	event.target.removeEventListener("mouseover", setGreyText, false);
	
	if(!quiz.isGameStarted())
	{
		event.target.setAttribute("style", "color: red;");
		if(quiz.getChoosenCatalog() != null && quiz.getChoosenCatalog() != event.target)
		{
			quiz.getChoosenCatalog().addEventListener("mouseover", setGreyText, false);
			quiz.getChoosenCatalog().addEventListener("mouseout", setBlackText, false);
			quiz.getChoosenCatalog().setAttribute("style", "color: black;");
		}
		quiz.setChoosenCatalog(event.target);
	}
}

function setGreyText(event)
{
	event.target.setAttribute("style", "color: grey;");
}

function setBlackText(event)
{
	event.target.setAttribute("style", "color: black;");
}