/**
 * 
 */

function Quiz()
{		
	var playerName = null;
	var playerID = null;
	var superUser = false;
	
	var choosenCatalog = null;
	var catalogSelection = null;
	var catalog = null;
	
	var socket = null;
	var connected = false;
	
	var gameStarted = false;
	
	var firstQuestion = true;
	var choosenAnswer = null;
	
	this.setPlayerName = function(_playerName){playerName = _playerName;};
	this.setChoosenCatalog = function(_choosenCatalog){choosenCatalog = _choosenCatalog;};
	this.setCatalogSelection = function(_catalogSelection){catalogSelection = _catalogSelection;};
	this.setCatalog = function(_catalog){catalogSelection = _catalog;};
	this.setSocket = function(_socket){socket = _socket;};
	this.setConnected = function(_connected){connected = _connected;};
	this.setGameStarted = function(_started){gameStarted = _started;};
	this.setSuperuser = function(_superUser){superUser = _superUser;};
	this.setPlayerID = function(_id){playerID = _id;};
	this.setChoosenAnswer = function(_coosenAnswer){choosenAnswer = _coosenAnswer;};
	this.setFirstQuestion = function(_firstQuestion){firstQuestion = _firstQuestion;};
	
	this.getPlayerName = function(){				return playerName;};
	this.getChoosenCatalog = function(){			return choosenCatalog;};
	this.getCatalogSelection = function(){			return catalogSelection;};
	this.getCatalog = function(){					return catalog;};
	this.getSocket = function(){					return socket;};
	this.getPlayerID = function(){					return playerID;};
	this.getChoosenAnswer = function(){				return choosenAnswer;};
	
	this.isConnected = function(){					return connected;};
	this.isSuperUser = function(){					return superUser;};
	this.isGameStarted = function(){				return gameStarted;};
	this.isFirstQuestion = function(){				return firstQuestion;};
}

var quiz = new Quiz();