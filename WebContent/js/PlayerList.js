function playerList()
{
	var source = new EventSource('SSEPlayerList');
	
	source.addEventListener('message', setPlayerList, false);
	source.addEventListener('error', errorListener, false);
}

function setPlayerList(event)
{
	clearPlaylist();
	var messageJSONArray = JSON.parse(event.data);
	
	var liName = document.getElementsByName("PlayerListPlayer");
	var liScore = document.getElementsByName("score");
	
	for(var i = 0; i < messageJSONArray.length; i++)
	{	
		liName[i].innerHTML = messageJSONArray[i].name;
		liScore[i].innerHTML = messageJSONArray[i].score;	
	}
}

function errorListener(event)
{
if (event.readyState == EventSource.CLOSED) 
	{
		console.log("SSE Verbindung zu");
	}
}

function clearPlaylist()
{
	var liName = document.getElementsByName("PlayerListPlayer");
	var liScore = document.getElementsByName("score");
	for(var j = 0; j < liName.length; j++)
	{
		liName[j].innerHTML ="";
		liScore[j].innerHTML ="";
	}
}