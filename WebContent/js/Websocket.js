/**
 * 
 */
document.addEventListener("DOMContentLoaded", createWebsocket, false);

function createWebsocket()
{
	document.getElementById("loginButton").addEventListener("click", send, false);
	var url = 'ws://localhost:8080/WebProg14/socket/Websocket';
//	var url = 'ws://fbe-wwwdev.hs-weingarten.de:8080/webprog08/tomcat/socket/Websocket'; 
	
	quiz.setSocket(new WebSocket(url));
	quiz.getSocket().onmessage = receive;
	quiz.getSocket().onopen = open;
	quiz.getSocket().onclose = close;
	quiz.getSocket().onerror = error;
}

function error(){
	console.log("Fatal error in Websocket");
}

function close() {
	 if(!quiz.isConnected()) {
		 console.log("Websocket disconnected without being opened, i.e. timeout");
	    } else {
	     console.log("disconnected whilst it had been opened before, i.e. connection lost");
	    }
	 	quiz.setConnected(false);
}

function open() {
	console.log("Socket offen");
	quiz.setConnected(true);
}

