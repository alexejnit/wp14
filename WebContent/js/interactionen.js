/*
 *
 */
document.addEventListener("DOMContentLoaded", init, false);
var interact = new Interactions(); 

function init()
{
	interact.getElements();
	
	interact.addMouseClickListenerLoginTextField();
	interact.addMouseOverListenerPlayArea();
	interact.addMouseOutListenerPlayArea();
	
	interact.addMouseOverListenerCatalogField();
	interact.addMouseOutListenerCatalogField();
}
/*
 * Klasse Interactions
 */
function Interactions()
{
	var loginTextField;
	var playArea;
	var catalogField;
	var choosenCatalog;
	
	this.addMouseClickListenerLoginTextField = addMouseClickListenerLoginTextField;
	
	this.removeMouseOutListenerLoginTextField =removeMouseOutListenerLoginTextField;
	
	this.addMouseOverListenerPlayArea = addMouseOverListenerPlayArea;
	this.addMouseOutListenerPlayArea = addMouseOutListenerPlayArea;
	
	this.addMouseOverListenerCatalogField = addMouseOverListenerCatalogField;
	this.addMouseOutListenerCatalogField = addMouseOutListenerCatalogField;
	
	this.getElements = getElements;
	
	this.getLoginTextField = function(){return this.loginTextField;}
	this.getPlayArea = function(){return this.playArea;}
	this.getCatalogField = function(){return this.catalogField;}
}

/*
 * Funktionen der Klasse Interactions
 */
function getElements()
{
	this.loginTextField = document.getElementById("loginTextField");
	this.playArea = document.getElementById("playArea_login");
	this.catalogField = document.getElementById("katalog");	
	this.choosenCatalog = null;
}

function addMouseClickListenerLoginTextField()
{
	this.loginTextField.addEventListener("focus", deleteTextFieldValue, true);
}

function removeMouseOutListenerLoginTextField()
{
	this.loginTextField.removeEventListener("mouseout", setTextFieldValue, false);
}

function addMouseOverListenerPlayArea()
{
	this.playArea.addEventListener("mouseover", setBiggerPlayArea, false);
}

function addMouseOutListenerPlayArea() 
{
	this.playArea.addEventListener("mouseout", setBackPlayArea, false);
}

function addMouseOverListenerCatalogField()
{
	this.catalogField.addEventListener("mouseover", setBiggerCatalogField, false);
}

function addMouseOutListenerCatalogField() 
{
	this.catalogField.addEventListener("mouseout", setBackCatalogField, false);
}



function deleteTextFieldValue(event)
{
	document.getElementById("loginTextField").setAttribute("value", "");
}

function setBiggerPlayArea(event)
{
	document.getElementById("playArea_login").setAttribute("style", "position:absolute; top: 15.5%; left: 2%; width:47%; height:66%;");
}

function setBackPlayArea(event)
{
	document.getElementById("playArea_login").setAttribute("style", "position:absolute; top: 16%; left: 2.5%; width:46%; height:65%;");
}

function setBiggerCatalogField(event)
{
	document.getElementById("katalog").setAttribute("style", "position:absolute; top: 15.5%; left: 54.5%; width:15.5%; height:30.5%;");
}

function setBackCatalogField(event)
{
	document.getElementById("katalog").setAttribute("style", "position:absolute; top: 16%; left: 55%; width:14.5%; height:29.5%;");
}

